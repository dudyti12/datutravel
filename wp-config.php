<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'datutravel' );

/** MySQL database username */
define( 'DB_USER', 'datuadmin' );

/** MySQL database password */
define( 'DB_PASSWORD', 'datu _#123' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'a Cs? R^5,=ZmP;$lC%JN5Cj/lSn3JLN)|P)le k:3_(c9s[t2SY@gJlT&S&#]:]' );
define( 'SECURE_AUTH_KEY',  '-hR&R$F%Gv}3y&Z>!$RV?v5$aL[bxke,`iiK|PZA)>,$sD,vX{AauvA<rtA<O8Y{' );
define( 'LOGGED_IN_KEY',    'UCyuCjGs4czxv=.9p;.D|4]lD+%|Sfu9[jY-h^3$No?>)Q3w&s_|=IM i9DGHQ(q' );
define( 'NONCE_KEY',        '^ORr8f<CE^Q?3l2!~n7.7O<)EUiI,qTo!@5N:afJ.MDUHt6Hs%#N6HuK3^?eVw![' );
define( 'AUTH_SALT',        ')BPY@GQ^S]s1-.-:F(_7[dq~g<<QC!gx5PLB7+p}RTGmKuq/jb1t!@-a 4xn^Qf]' );
define( 'SECURE_AUTH_SALT', 'mySg2=nA_8b=!T)<#*S]Wo, ujPTx`T%B:)0n(81Yy=4I}xp~j_3wdRk`-tyaaI(' );
define( 'LOGGED_IN_SALT',   '^&f3Dx.MwWx6Xqur5.:uzn18O5wV>ELftx0{jqZ^j2vURUP:sVI!sfn0R=E68!9Y' );
define( 'NONCE_SALT',       'KB=^3XXb2g~D7o6{2~pgd5x2w-(_=U5i|22_}nK6=S8B^$CViNj-^3/5?MN`nJEU' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
